package com.paic.arch.interviews;


/**
 * Created by chenlunwei on 2018/3/6.
 * TimeConverter继承类，实现convertTime方法
 */
public class TimeConverterImpl implements TimeConverter {
	//创建TimeConverterImpl的唯一实例
	private static final TimeConverter TIMECONVERTER = new TimeConverterImpl();

	//构造器私有化，避免通过new的方式创建实例
	private TimeConverterImpl() {
	}

	//通过静态方法获取TimeConverterImpl的唯一实例
	public static TimeConverter getTimeConverter() {
		return TIMECONVERTER;
	}

	/**
	 * 把时间转换成需要的格式的方法
	 * @param aTime
	 * @return
	 */
	@Override
	public String convertTime(String aTime) {
		String[] times = aTime.split(":");
		if (times != null && times.length > 0) {
			String second = second(times[2]);
			String singleMinute = singleMinute(times[1]);
			String tenMinutes = tenMinutes(times[1]);
			String singleHour = singleHour(times[0]);
			String fiveHours = fiveHours(times[0]);
			return second + "\n" + fiveHours + "\n" + singleHour + "\n"
					+ tenMinutes + "\n" + singleMinute;
		}
		return null;
	}

	/**
	 * 生成第一层数据，当时间的秒数为偶数时，返回"Y",否则返回"O"
	 *
	 * @param secondTime 秒数
	 * @return 返回第一层数据
	 */
	public String second(String secondTime) {
		return Integer.parseInt(secondTime) % 2 == 0 ? "Y" : "O";
	}

	/**
	 * 通过小时数计算出第二层的亮灯数，然后通过covert方法生成第二层数据
	 * @param hoursTime 小时数
	 * @return 返回第二层数据
	 */
	public String fiveHours(String hoursTime) {
		int time = Integer.parseInt(hoursTime);
		int ligthOnNum = (time - time % 5) / 5;
		return covert(4, ligthOnNum, 'R');
	}

	/**
	 * 通过小时数计算出第三层的亮灯数，然后通过covert方法生成第三层数据
	 * @param hoursTime
	 * @return 返回第三层数据
	 */
	public String singleHour(String hoursTime) {
		return covert(4, Integer.parseInt(hoursTime) % 5, 'R');
	}

	/**
	 * 通过分钟数计算出第四层的亮灯数，然后通过covert方法生成第四层数据
	 * @param minutesTime
	 * @return 返回第四次数据
	 */
	public String tenMinutes(String minutesTime) {
		int time = Integer.parseInt(minutesTime);
		int ligthOnNum = (time - time % 5) / 5;
		return covert(11, ligthOnNum, 'Y').replace("YYY", "YYR");
	}

	/**
	 * 通过分钟数计算出第五层的亮灯数，然后通过covert方法生成第四层数据
	 * @param minutesTime 分钟数
	 * @return 第五层数据
	 */
	public String singleMinute(String minutesTime) {
		return covert(4, Integer.parseInt(minutesTime) % 5, 'Y');
	}

	/**
	 * 根据灯的数量，处于亮灯状态的数量以及灯的颜色生成数据
	 *
	 * @param lightNum   该层灯的数量
	 * @param ligthOnNum 处于亮灯状态的灯的数量
	 * @param color      处于亮灯状态的灯的颜色
	 * @return 返回该层所有灯得颜色状态
	 */
	public String covert(int lightNum, int ligthOnNum, char color) {
		String result = "";
		for (int i = 0; i < ligthOnNum; i++) {
			result += color;
		}
		for (int i = ligthOnNum; i < lightNum; i++) {
			result += "O";
		}
		return result;
	}
}
